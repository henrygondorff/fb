import jwt from 'jsonwebtoken'
import {Response} from "express";


function authenticate(req:any ,res: any,next: any) {
   try {
     const token = req.header('Authorization').replace('Bearer ', '')
      console.log(token)
       const userId = jwt.verify(token, 'secret')
       console.log(userId)
       req.userId = userId
       next()
   }catch (e) {
       console.log(e)
      res.status(401).json({message: 'NOT_AUTH'})
   }

}

export default authenticate
