export interface User {
    id: number,
    email: string,
    firstName : string,
    lastName : string,
    createdAt : Date,
    password? : string
}




export interface LoginUser {
    email: string,
    password: String
}

export interface RegisterUser {
    email: string,
    firstName : string,
    lastName : string,
    password : string
}

