export type Post = PostCreateRequest & { id: number,  createdAt: Date, reactionsCount: number}


export interface PostCreateRequest {
  text: string
}
