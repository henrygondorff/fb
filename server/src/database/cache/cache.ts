import redis, {RedisClient} from "redis"

const port_redis = 6379;


class CacheService {
    cache: RedisClient

    constructor() {
        this.cache = redis.createClient(port_redis);
    }

    setData(id: string, data: any, ttlS = 15,) {
        console.log('cache:', id, data)
        this.cache.setex(id, ttlS, JSON.stringify(data));
    }

    checkCache = (id: string) => {
        return new Promise((resolve, reject) => {
                this.cache.get(id, (err, data) => {
                        if (err) {
                            console.log(err);
                            reject(err)
                        }
                        if (data) {
                            resolve(JSON.parse(data))
                        }
                        resolve(null)
                    }
                )
            }
        )


        //get database value for key =id


    };
}

const cacheService = new CacheService()

export default cacheService
