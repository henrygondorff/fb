const createReaction = `INSERT INTO Reactions (type, parentId) VALUES(?,?)`

const getReactionByPostId = `SELECT * FROM Reactions WHERE parentId = ?`
const getReactionCountByPostId = `SELECT count(*) FROM Reactions WHERE parentId = ?`

export default {createReaction,getReactionByPostId,getReactionCountByPostId  }
