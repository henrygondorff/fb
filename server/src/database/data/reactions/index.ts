import db from "../../connection"
import {Post, PostCreateRequest} from "../../../models/post.model";
import queries from './queries'
import {CreateReactionRequest, Reaction} from "../../../models/reaction.model";


async  function createReaction(reaction: CreateReactionRequest): Promise<Number[]> {
    try {
         await new Promise((resolve, reject) => {
            db.query(queries.createReaction,
                [reaction.type, reaction.parentId],
                (err, data) => {
                    if (err) {return reject(err)}
                    return resolve(data.insertId)
                })})
        return await getReactionCountPostId(reaction.parentId)
    }catch (e) {
        return Promise.reject(500)
    }
}

async  function getReactionByPostId(postId: number): Promise<Reaction[]> {
    return await new Promise((resolve, reject) => {
        return db.query(queries.getReactionByPostId,
            [postId],
            (err, data) => {
                if (err) {return reject(err)}
                resolve(data)})})
}

async  function getReactionCountPostId(postId: number): Promise<Number[]> {
    return await new Promise((resolve, reject) => {
        return db.query(queries.getReactionCountByPostId,
            [postId],
            (err, data) => {
                if (err) {return reject(err)}
                resolve(data[0]['count(*)'])})})
}



export default {createReaction}
