const create = `INSERT INTO Post (text) VALUES(?)`;

const selectById = `SELECT * FROM Post WHERE id=?`

const selectAll = `
SELECT Post.*, Count(Reactions.id) as reactionsCount FROM Post
LEFT JOIN  Reactions ON Post.id = Reactions.parentId
group by 1

`

export default {create,selectById,selectAll }
