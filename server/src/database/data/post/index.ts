import db from "../../connection"
import {Post, PostCreateRequest} from "../../../models/post.model";
import queries from './queries'
import {CreateReactionRequest, Reaction} from "../../../models/reaction.model";


async function createPost(post: PostCreateRequest): Promise<Post> {
    try {
        const id : number = await new Promise((resolve, reject) => {
            db.query(queries.create,
                [post.text],
                (err, data) => {
                    if (err) {return reject(err)}
                    return resolve(data.insertId)
                })})
        return await getPostById(id) as Post
    }catch (e) {
       return Promise.reject(500)
    }
}

async  function getPostById(id: number): Promise<Post> {
    return await new Promise((resolve, reject) => {
        return db.query(queries.selectById,
            [id],
            (err, data) => {
                if (err) {return reject(err)}
                console.log(data)
                if(!data.length){
                    reject(404)
                }
                resolve(data[0])})})
}
async  function getPosts(): Promise<Post[]> {
    return await new Promise((resolve, reject) => {
        return db.query(queries.selectAll,
            (err, data) => {
                if (err) {return reject(err)}
                resolve(data)})})
}

export default {createPost, getPosts}
