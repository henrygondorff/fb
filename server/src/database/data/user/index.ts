import db from "../../connection"
import {Register} from "ts-node";
import {RegisterUser, User} from "../../../models/user.model";
import {Post} from "../../../models/post.model";
import queries from "./queries";

async function registerUser(user: RegisterUser): Promise<User> {
    try {
        const id: number = await new Promise((resolve, reject) => {
            db.query(queries.register,
                    [user.email, user.firstName, user.lastName, user.password],
                (err, data) => {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(data.insertId)
                })
        })
        return getUserById(id)
    } catch (e) {
        return Promise.reject(500)
    }
}

async function emailExist(email : string) : Promise<boolean> {
    try {
        return await new Promise((resolve, reject) => {
            db.query(queries.emailExist,
                [email],
                (err, data) => {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(data.length > 0)
                })
        })
    } catch (e) {
        console.error(e)
        return Promise.reject(500)
    }
}

async function getUserById(userId: number) : Promise<User> {
    try {
        return await new Promise((resolve, reject) => {
            db.query(queries.getById,
                [userId],
                (err, data) => {
                    if (err) {
                        return reject(err)
                    }
                    if(data.length > 0){
                        return resolve(data[0])
                    }else {
                        return  Promise.reject(404)
                    }

                })
        })
    } catch (e) {
        console.error(e)
        return Promise.reject(500)
    }
}


async function getUserByEmail(email: string) : Promise<User> {
    try {
        return await new Promise((resolve, reject) => {
            db.query(queries.getByEmail,
                [email],
                (err, data) => {
                    if (err) {
                        return reject(err)
                    }
                    if(data.length > 0){
                        return resolve(data[0])
                    }else {
                        return  Promise.reject(null)
                    }

                })
        })
    } catch (e) {
        console.error(e)
        return Promise.reject(500)
    }
}



export default {registerUser, emailExist,getUserByEmail}
