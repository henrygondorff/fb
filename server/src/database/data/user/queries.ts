const register = `INSERT INTO User (email,firstName,lastName,password) VALUES(?,?,?,?)`;
const emailExist = `SELECT * FROM User WHERE email = ?`
const getById = `SELECT * FROM User WHERE id = ?`
const getByEmail = `SELECT * FROM User WHERE email = ?`


export default {register,emailExist, getById, getByEmail}
