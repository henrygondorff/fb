import express from "express";
import {LoginUser, RegisterUser} from "../../models/user.model";
import userService from "../../services/user-service";

const router = express.Router();




router.post('/', async (req, res, next) => {
    const userToRegister: RegisterUser = req.body

    //Validera inputs
    try {
        const user = await userService.register(userToRegister);
        res.status(200).json(user)
    } catch (error) {
        console.error(error)
        if(error.status){
            res.status(error.status).json({message: error.message})
        }else {
            res.status(500).send()
        }
    }
})

router.post('/login', async (req, res, next) => {
    const userToLogin: LoginUser = req.body



    //Validera inputs
    try {
        const user = await userService.login(userToLogin);
        res.status(200).json(user)
    } catch (error) {

        console.error(error)
        if(error.status){
            res.status(error.status).json({message: error.message})
        }else {
            res.status(500).send()
        }

    }
})

export default router
