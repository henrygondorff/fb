import express from 'express'
import postService from "../services/post-service/post-service";
import reactionService from "../services/reaction-service/reaction-service";
import {Post} from "../models/post.model";
import {CreateReactionRequest, ReactionType} from "../models/reaction.model";

const router = express.Router();





router.post("/", async (req, res, next) => {
    const post = req.body

    try {
        const createdPost = await postService.createPost(post);
        res.status(200).json(createdPost)
    } catch (e) {
        console.error(e)
        res.status(500).send()
    }
});

router.get("/", async (req, res, next) => {
    try {
        const posts =  await postService.getPosts();
        res.status(200).json(posts)
    } catch (e) {
        console.error(e)
        res.status(500).send()
    }
});

router.post("/:id/reactions", async (req, res, next) => {
    const {type} : {type: ReactionType} = req.body
    const parentId = +req.params.id
    const reaction : CreateReactionRequest = {type, parentId}

    try {
        const reactions = await reactionService.addReaction(reaction);
        res.status(200).json(reactions)
    } catch (e) {
        console.error(e)
        res.status(500).send()
    }
});

export default router
