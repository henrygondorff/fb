import {Post, PostCreateRequest} from "../../models/post.model";
import reactionDatabase from '../../database/data/reactions'
import {CreateReactionRequest, Reaction} from "../../models/reaction.model";




class ReactionService {
    addReaction(reaction: CreateReactionRequest) : Promise<Number[]> {
        return reactionDatabase.createReaction(reaction)
    }
}

export default new ReactionService()
