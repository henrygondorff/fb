import {User, RegisterUser, LoginUser} from "../../models/user.model"
import userDatabase from '../../database/data/user'
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken'

class UserService {
    async register(userToRegister: RegisterUser): Promise<{user: User, token: string}> {
        userToRegister.password = await bcrypt.hash(userToRegister.password, 8)
        const emailExist = await userDatabase.emailExist(userToRegister.email);
        if (emailExist) {
            return Promise.reject({status: 409, message: 'EMAIL_IN_USE'})
        }

        const user = await userDatabase.registerUser(userToRegister)
        delete user.password
        const token = this.generateAuthToken(user)
        return {user, token}
    }

    async login(userToLogin: LoginUser): Promise<{user: User, token: string}> {
        const user = await userDatabase.getUserByEmail(userToLogin.email);
        if(!user){
            return Promise.reject({status: 403, message: 'INVAILD_CREDENTIALS'})
        }

        const isValidPassword = await bcrypt.compare(userToLogin.password, user.password!)
        if(! isValidPassword){
            return Promise.reject({status: 403, message: 'INVAILD_CREDENTIALS'})
        }



        delete user.password
        const token = this.generateAuthToken(user)
        return {user, token}
    }


    private generateAuthToken(user: User){
        return  jwt.sign({id: user.id.toString()}, 'secret')
    }
}

export default new UserService()
