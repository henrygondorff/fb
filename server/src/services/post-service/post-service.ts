import {Post, PostCreateRequest} from "../../models/post.model";
import PostDatabase from '../../database/data/post'
import {CreateReactionRequest, Reaction} from "../../models/reaction.model";




 class PostService {
    createPost(postToCreate: PostCreateRequest) : Promise<Post> {
        return PostDatabase.createPost(postToCreate)
    }

    getPosts() : Promise<Post[]> {
        return PostDatabase.getPosts()
    }
}

export default new PostService()
