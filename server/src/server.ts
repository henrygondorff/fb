import app from './app'

const PORT = 8080

async function startServer() {
    app.listen(PORT, () => {
        console.log(`Server listening at port ${PORT}`)
    });
}

startServer();
