import express from 'express'
import cors from 'cors'
import postRoutes from './routes/post-routes'
import userRoutes from './routes/user-routes'
import path from "path";
import helmet from 'helmet'
import morgan from 'morgan'
import authenticate from "./middleware/authenticate";
const app = express();
morgan('tiny')
app.use(helmet());
app.use(cors())
app.use(express.json());
app.use(express.static(path.join(__dirname, '../client/build')));
app.use("/posts",authenticate, postRoutes);
app.use("/users", userRoutes);

app.get('*', (req,res) =>{
    res.sendFile(path.join(__dirname, '../client/build/index.html'));
});
export default app;
