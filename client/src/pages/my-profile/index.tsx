import React from "react";
import MyProfileContainer from "../../containers/my-profile-container";

export default function MyProfile() {
    return (
        <div style={{width: '40%'}}>
            <MyProfileContainer/>
        </div>)
}
