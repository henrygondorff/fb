import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Root from "./root";
import {GlobalStyles} from "./global-styles/global-styles";
import {ThemeProvider} from "styled-components";
import theme from "./global-styles/theme";


ReactDOM.render(
    <React.StrictMode>
        <ThemeProvider theme={theme}>
            <GlobalStyles/>
            <Root/>
        </ThemeProvider>
    </React.StrictMode>,
    document.getElementById('root'));

