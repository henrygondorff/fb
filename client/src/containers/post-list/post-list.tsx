import {Post} from "../../../../server/src/models/post.model";
import React, {FC, FunctionComponent, useCallback} from "react";
import useAsync from "../../hooks/use-async";
import {NewPost} from "../../services/post/models/post.model";
import postService from "../../services/post/post.service";
import PostCard from "../../components/post-card/post-card";
import {usePosts} from "../../context/posts/post-provider";

const PostList = ()  => {
        const {posts} = usePosts()

        return <div> {posts.map(post  => <PostCard key={post.id} margin post={post}/>)} </div>
}

export default PostList
