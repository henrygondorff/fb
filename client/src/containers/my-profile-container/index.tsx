import React from "react";
import AddPostCard from "../../components/add-post-card";
import PostCard from "../../components/post-card/post-card";
import PostList from "../post-list/post-list";
import {PostProvider} from "../../context/posts/post-provider";

export default function MyProfileContainer()  {
    return <>

<PostProvider>
    <AddPostCard/>
    <PostList/>
</PostProvider>
    </>
}
