import React, {ReactNode} from 'react'
import styled from  './styled'


interface IconButtonProps {
    className : string;
    children: ReactNode;
    onClick : () => void;
}

 const IconButton = ({className, onClick, children} : IconButtonProps) => {
    return (
      <styled.IconButton onClick={onClick} className={className}>
          {children}
      </styled.IconButton>
    );
};


export default IconButton
