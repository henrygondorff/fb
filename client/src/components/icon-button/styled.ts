import styled from 'styled-components/macro'

const IconButton = styled.button`
  width: 36px;
  height: 36px;
  border-radius: 100%;
  background: ${({theme}) => theme.color.tertiary};
  border: none;
  outline: none;
  display: flex;
  align-items: center;
  justify-content: center;
  
  
  
  &:hover {
    cursor: pointer; 
    background: ${({theme}) => theme.color.tertiaryDark};
  }
`

export default {IconButton}
