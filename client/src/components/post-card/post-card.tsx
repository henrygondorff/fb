import CardCanvas from "../card-canvas";
import React from "react";
import ProfileIcon from "../profile-icon";
import {Post} from "../../../../server/src/models/post.model";
import Button from "../button";
import {MdChatBubbleOutline, MdThumbUp} from 'react-icons/md';
import {formatDistanceToNow} from 'date-fns'
import Note from "../note";
import {usePosts} from "../../context/posts/post-provider";
import {ReactionType} from "../../services/post/models/reaction.model";
import {Theme} from "../../styled"
import {IconContext} from "react-icons";


interface PostCardProps {
    post: Post,
    margin?: boolean,
}


const PostCard = ({post, margin}: PostCardProps) => {
    const {addReaction} = usePosts()

    return (
        <CardCanvas margin>
            <CardCanvas.Row>
                <ProfileIcon
                    profileImage="https://scontent.farn2-1.fna.fbcdn.net/v/t1.30497-1/cp0/c24.0.80.80a/p80x80/84241059_189132118950875_4138507100605120512_n.jpg?_nc_cat=1&_nc_sid=7206a8&_nc_ohc=aOPdmOcZhHkAX92otbs&_nc_ht=scontent.farn2-1.fna&_nc_tp=27&oh=3ce031c1ea10dd108da5b08bfa0ac9a2&oe=5FA8E881"/>
                <div>
                    <p>Andreas Ivarsson</p>
                    <Note>{formatDistanceToNow(new Date(post.createdAt))} ago </Note>
                </div>
            </CardCanvas.Row>
            <CardCanvas.Row line margin>
                <p>
                    {post.text}</p>
            </CardCanvas.Row>
            <CardCanvas.Row >
                <IconContext.Provider value={{className: "icon-fill"}}>
                    <MdThumbUp size={12}/>
                </IconContext.Provider> <span>{post.reactionsCount}</span>
            </CardCanvas.Row>
            <CardCanvas.Row center style={{maxWidth: '400px'}}>
                <Button onClick={() => addReaction({type: ReactionType.LIKE, parentId: post.id})} fill='clear'>
                    <IconContext.Provider value={{className: post.reactionsCount % 2 === 0?"icon-outline": ''}}>
                    <MdThumbUp size={20}/>
                    </IconContext.Provider>
                    Gilla
                </Button>
                <Button fill='clear'>
                    <MdChatBubbleOutline size={20}/>
                    Kommentera
                </Button>
            </CardCanvas.Row>
        </CardCanvas>

    )
}

export default PostCard
