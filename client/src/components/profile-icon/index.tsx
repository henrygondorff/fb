import React from 'react'
import styled from './styled'


interface AddPostCardProps {
    profileImage: string
}

const ProfileIcon = ({profileImage}: AddPostCardProps) => {
    return <styled.ProfileIcon src={profileImage}/>
}


export default ProfileIcon
