import styled from "styled-components/macro";
import theme from "../../global-styles/theme";


const ProfileIcon = styled.img`
  border-radius: 50%; 
  width: 40px;
  height: 40px;
  margin-right: 10px;
 
`


export default {ProfileIcon}
