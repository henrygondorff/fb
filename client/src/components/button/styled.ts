import styled from "styled-components/macro";
import {Theme} from "../../styled";
import {inherits} from "util";

interface Props {
    theme: Theme,
    fill?: 'solid' | 'clear'
}

const button = styled.button<Props>`
 border: none;
 outline: none;
 border-radius: 6px;
 text-align: center;
 display: flex;
 align-items: center;
 justify-content: center;
 width: 100%;
 padding: 10px;
 font-size: 1rem;
 background: ${({fill, theme}) => fill === 'clear' ? 'transparent' : theme.color.primary};
 color: ${({fill, theme}) => fill === 'clear' ?  theme.color.secondaryDark : theme.color.primaryContrast};
 
 & > * {
  margin: 4px;
 }
 
 &:hover {
  background: ${({fill,theme}) => fill === 'clear' ? theme.color.tertiaryDark :theme.color.primaryDark};
 cursor: pointer;
 }
 
 &:disabled { 
 background: ${({theme}) => theme.color.tertiary};
 color: ${({theme}) => theme.color.tertiaryDark};
 cursor: default;
 }
`

export default {button}
