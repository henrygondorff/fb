import React, {ReactNode} from 'react'
import styled from "./styled";

interface ButtonProps {
    children: ReactNode;
    fill? : 'solid' | 'clear'
    [key: string]: any
}

const Button = ({children, ...rest}: ButtonProps) => {
    return (
        <styled.button {...rest}>
            {children}
        </styled.button>

    )
}


export default Button
