import React, {ReactElement, ReactNode} from 'react'
import styled from './styled'

interface NoteProps {
    color?: string,
    children? : ReactNode
}

const Note = ({color,children}: NoteProps) => <styled.Note color={color}>{children}</styled.Note>

export default Note
