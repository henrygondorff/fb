import styled from 'styled-components/macro'

interface NoteProps {
    color?: string
}

const Note = styled.span<NoteProps>`
   font-size: 0.8rem;
   color: ${({color,theme}) => color && theme.color.secondaryDark }
`

export default {Note}
