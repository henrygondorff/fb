import styled from "styled-components/macro";
import theme from "../../global-styles/theme";
import {rgba} from 'polished'


const Backdrop = styled.div`
 position: absolute;
 top: 0;
 bottom: 0;
 left: 0;
 right: 0;
 background: ${({theme}) => rgba(theme.color.secondary, 0.7)};
`

const ModalContainer = styled.div`
 position: absolute;
 top: 0;
 bottom: 0;
 left: 0;
 right: 0; 
 display: flex;
 align-items: center;
 justify-content: center; 
`

const ModalHeader = styled.div`
 color:  ${({theme}) => theme.color.tertiaryContrast} ; 
 width: 500px; 
 position: relative; 
 height: 35px;
 padding-bottom: 7px;
 display: flex;
 align-items: center;
 justify-content: center;
 border-bottom: 1px solid  ${({theme}) => theme.color.tertiaryDark};
 
 & * {
  font-size: 1.3rem;
 }
 
 & .right {
 position: absolute;
 right: 0;
 }
`

const TextArea = styled.textarea`
width: 100%;
resize: none;
border: none;
outline: none;
font-size: 1.3rem;

`


export default {ModalContainer, ModalHeader, Backdrop, TextArea};
