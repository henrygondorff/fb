import React, {useEffect, useState} from 'react'
import CardCanvas from "../card-canvas";
import styled from './styled'
import ProfileIcon from "../profile-icon";
import IconButton from "../icon-button";
import {MdClose} from 'react-icons/md';
import Button from "../button";
import postService from '../../services/post/post.service'
import {NewPost} from '../../services/post/models/post.model'
import useAsync from '../../hooks/use-async'
import {ToastContainer, toast} from 'react-toastify';
import {usePosts} from "../../context/posts/post-provider";
import {Post} from "../../../../server/src/models/post.model";


interface AddPostModalProps {
    close: () => void
}

const AddPostModal = ({close}: AddPostModalProps) => {
    const {addPost} = usePosts()
    const [postText, setPostText] = useState('')
    const {execute: createNewPost, status, value: post, error} = useAsync<Post>((post: NewPost) => postService.addPost(post), [{text: postText} as NewPost], false);


    const submitPost = () => {
        createNewPost()
    }

    useEffect(() => {
        if (status === 'success' && post) {
            addPost(post)
            close()
        }
    }, [status])


    const postTextInvalid = !postText

    return (
        <>

            <styled.Backdrop/>
            <styled.ModalContainer>
                <CardCanvas>
                    <CardCanvas.Row>
                        <styled.ModalHeader>
                            <h2>Skapa Inlägg</h2>
                            <IconButton onClick={close} className='right'>
                                <MdClose size={26}/>
                            </IconButton>
                        </styled.ModalHeader>

                    </CardCanvas.Row>
                    <CardCanvas.Row>
                        <ProfileIcon
                            profileImage="https://scontent.farn2-1.fna.fbcdn.net/v/t1.30497-1/cp0/c24.0.80.80a/p80x80/84241059_189132118950875_4138507100605120512_n.jpg?_nc_cat=1&_nc_sid=7206a8&_nc_ohc=aOPdmOcZhHkAX92otbs&_nc_ht=scontent.farn2-1.fna&_nc_tp=27&oh=3ce031c1ea10dd108da5b08bfa0ac9a2&oe=5FA8E881"/>
                        <p>Andreas Ivarsson</p>
                    </CardCanvas.Row>
                    <CardCanvas.Row>
                        <styled.TextArea value={postText} onChange={(e) => setPostText(e.target.value)}
                                         placeholder="Vad gör du just nu?" rows={5}/>
                    </CardCanvas.Row>
                    <CardCanvas.Row>
                        <Button disabled={postTextInvalid} onClick={submitPost}>Publicera</Button>
                    </CardCanvas.Row>
                </CardCanvas>
            </styled.ModalContainer>
        </>)
}


export default AddPostModal
