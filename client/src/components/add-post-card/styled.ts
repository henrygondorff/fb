import styled from "styled-components/macro";
import theme from "../../global-styles/theme";




const InputPlaceholder = styled.div`
  background: ${({theme}) => theme.color.tertiary} ;
  border-radius: 20px; 
  height: 40px;
  flex:1;
  margin-left: 10px;
  display: flex;
  align-items: center;
  padding-left: 10px;
  
  &:hover {
    cursor: pointer;
    background: ${({theme}) => theme.color.tertiaryDark} ;
  }
`

export default {InputPlaceholder}
