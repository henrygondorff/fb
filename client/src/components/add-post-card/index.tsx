import React, {useState} from 'react'
import CardCanvas from "../card-canvas";
import styled from './styled'
import AddPostModal from "../add-post-modal";
import ProfileIcon from "../profile-icon";

const AddPostCard = () => {
    const [showModal, setShowModal] = useState(false)

    return <CardCanvas>
        {showModal && <AddPostModal close={() => setShowModal(false)}/>}
        <CardCanvas.Row>
            <ProfileIcon profileImage="https://scontent.farn2-1.fna.fbcdn.net/v/t1.30497-1/cp0/c24.0.80.80a/p80x80/84241059_189132118950875_4138507100605120512_n.jpg?_nc_cat=1&_nc_sid=7206a8&_nc_ohc=aOPdmOcZhHkAX92otbs&_nc_ht=scontent.farn2-1.fna&_nc_tp=27&oh=3ce031c1ea10dd108da5b08bfa0ac9a2&oe=5FA8E881"/>
            <styled.InputPlaceholder onClick={()=> setShowModal(true)}>Vad gör du just nu? </styled.InputPlaceholder>
        </CardCanvas.Row>
    </CardCanvas>
}


export default AddPostCard
