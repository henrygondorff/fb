import styled from "styled-components/macro";
import {Theme} from "../../styled";

const CardCanvas = styled.div`
    background: ${({theme}) => theme.color.secondary};
    color: ${({theme}) => theme.color.secondaryContrast};
    padding: 12px 16px 10px 16px;
    border-radius: 8px; 
    box-shadow: 0 1px 2px rgba(0,0,0,0.3); 
    display: flex;
    flex-direction: column;
    margin-bottom:  ${({margin}: { margin: boolean }) => margin ? '15px' : '0px'};;
    
`

interface RowProps {
    theme: Theme,
    line?: boolean,
    center?: boolean
    margin?: boolean
}

const CardCanvasRow = styled.div<RowProps>`
 border-bottom: ${({line, theme}) => line ? `1px solid ${theme.color.tertiaryDark}` : null};
 display: flex;
 align-items: center;
 justify-content:  ${({center}: { center?: boolean, theme: any }) => center ? 'center' : null};
 padding: 3px;
 padding-bottom:  ${({margin}) => margin ? '10px' : '3px'};;
  
  &:last-child {
    padding-bottom: 5px;
  }
   
`


export default {CardCanvas, CardCanvasRow}
