import React, {FunctionComponent, ReactNode} from "react";
import Styled from './styled'

interface CardCanvasProps {
     children: ReactNode;
     margin?: boolean
}

const CardCanvas = ({children,margin}:CardCanvasProps) =>
     <Styled.CardCanvas margin>
         {children}
     </Styled.CardCanvas>

CardCanvas.Row = Styled.CardCanvasRow

export default CardCanvas
