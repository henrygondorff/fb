
import theme from './global-styles/theme'

type Theme = typeof theme

declare module 'react-loading-overlay' {

}

declare module 'styled-components' {
    export interface DefaultTheme extends Theme {}
}
