export default {
    color: {
        primary: '#1672E6',
        primaryDark: '#1366ce',
        primaryLight: '#ffffff',
        primaryContrast: '#ffffff',

        secondary: '#ffffff',
        secondaryDark: '#737376',
        secondaryLight: '#ffffff',
        secondaryContrast: '#A4A4A4',

        tertiary: '#F0F2F5',
        tertiaryDark: '#DADDE1',
        tertiaryLight: '#ffffff',
        tertiaryContrast: '#050505',


    }
}
