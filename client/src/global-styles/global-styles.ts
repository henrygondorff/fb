import {createGlobalStyle} from 'styled-components/macro';
import {normalize} from 'styled-normalize'

export const GlobalStyles = createGlobalStyle`
   ${normalize}
   @import url('https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap');
   * {
   margin: 0;
   padding: 0;
   }
  html, body { 
  font-family: 'Roboto Condensed', sans-serif;
   font-size: 100%;
   box-sizing: border-box;
   background: ${props => props.theme.color.tertiary};
}
   p {
   font-size: 1rem;
   color:  ${props => props.theme.color.tertiaryContrast};
  word-break: break-all;
   }


.icon-fill {
   padding: 3px; 
   background:  ${props => props.theme.color.primary}; 
   color:  ${props => props.theme.color.primaryContrast}; 
   margin-right: 5px; 
   border-radius: 50%;
}

.icon-outline {
   color:  ${props => props.theme.color.primary}; 
}


`;
