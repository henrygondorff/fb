export interface Reaction extends CreateReactionRequest{
    id: number
}

export interface CreateReactionRequest {
    parentId: number,
    type: ReactionType
}

export enum ReactionType {
    LIKE = 'LIKE'
}
