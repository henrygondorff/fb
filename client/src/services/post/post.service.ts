import {NewPost} from "./models/post.model";
import apiService from "../api.service";
import {CreateReactionRequest, Reaction} from "../../../../server/src/models/reaction.model";

const path = 'posts'

const addPost = async (post: NewPost): Promise<any> => {
    return apiService.doPost(path, post)
}

const addReaction = async (reaction: CreateReactionRequest): Promise<any> => {
    return apiService.doPost(`${path}/${reaction.parentId}/reactions`, {type: reaction.type})
}


const getPosts = async () => {
    return apiService.doGet('posts')
}


export default {getPosts,addPost, addReaction}
