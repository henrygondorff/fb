const domain = 'http://localhost:8080'

const getHttpHeaders = () => {
    return {
        'Content-Type': 'application/json',
    }
};


const doPost = async (path: string, body: { [key: string]: any }) => {
    const response = await fetch(`${domain}/${path}`, {
        method: 'POST',
        headers: getHttpHeaders(),
        body: JSON.stringify(body)
    })

    if (response.ok){
        return response.json()
    }

    throw  new Error(response.statusText)
}

const doGet = async (path: string) => {
    const response =  await fetch(`${domain}/${path}`, {
        headers: getHttpHeaders(),
    })
    if(response.ok){
        return await response.json()
    }

    throw  new Error(response.statusText)

}

export default {doPost,doGet}
