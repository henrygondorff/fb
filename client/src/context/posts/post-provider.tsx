import {createContext, ReactNode, useContext, useEffect, useState} from "react";
import {Post} from "../../../../server/src/models/post.model";
import React from 'react'
import useAsync from "../../hooks/use-async";
import postService from "../../services/post/post.service";
import {NewPost} from "../../services/post/models/post.model";
import {CreateReactionRequest} from "../../services/post/models/reaction.model";


const PostContext = createContext<any>(null);

const PostProvider = ({ children }: {children: ReactNode}) => {
    const [posts, setPosts] = useState<Post[]>([]);
    const {value : getPosts, status :getStatus} = useAsync(postService.getPosts);

    const addPost = (post: Post) => {
        setPosts([post,...posts])
    }


    const addReaction = async (reaction: CreateReactionRequest) => {
        const reactionsCount = await postService.addReaction(reaction)
        const updatedPost =  posts.find(post => post.id === reaction.parentId)
        if(updatedPost){
            updatedPost.reactionsCount = reactionsCount
        }

        setPosts([...posts])

    }

    useEffect(() => {
        if(getStatus === 'success'){
            setPosts(getPosts as Post[])
        }
    },[getStatus])



    return (
        <PostContext.Provider value={{posts,addPost, addReaction}}>
            {children}
        </PostContext.Provider>
    );
};

const usePosts  = () => {
    const context = useContext<{posts: Post[], addPost:(post:Post)=> any, addReaction:(reaction : CreateReactionRequest) => void}>(PostContext);
    if (context === undefined) {
        throw new Error('usePosts can only be used inside PostProvider');
    }
    return context;
}

export { PostProvider,usePosts };

